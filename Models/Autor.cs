﻿using System;
using System.Collections.Generic;

#nullable disable

namespace efestudodecaso.Models
{
    public partial class Autor
    {
        public Autor()
        {
            AutoresProdutos = new HashSet<AutoresProduto>();
        }

        public decimal CodAutor { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }

        public virtual ICollection<AutoresProduto> AutoresProdutos { get; set; }
    }
}
