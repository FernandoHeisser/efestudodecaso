﻿using System;
using System.Collections.Generic;

#nullable disable

namespace efestudodecaso.Models
{
    public partial class Usuario
    {
        public decimal CodUsuario { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public virtual Administrador Administradore { get; set; }
        public virtual Cliente Cliente { get; set; }
    }
}
