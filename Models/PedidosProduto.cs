﻿using System;
using System.Collections.Generic;

#nullable disable

namespace efestudodecaso.Models
{
    public partial class PedidosProduto
    {
        public decimal NumPedido { get; set; }
        public decimal CodProduto { get; set; }
        public decimal Quantidade { get; set; }
        public decimal ValorUnitario { get; set; }

        public virtual Produto CodProdutoNavigation { get; set; }
        public virtual Pedido NumPedidoNavigation { get; set; }
    }
}
