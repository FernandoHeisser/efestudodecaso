﻿using System;
using System.Collections.Generic;

#nullable disable

namespace efestudodecaso.Models
{
    public partial class AutoresProduto
    {
        public decimal CodAutor { get; set; }
        public decimal CodProduto { get; set; }

        public virtual Autor CodAutorNavigation { get; set; }
        public virtual Produto CodProdutoNavigation { get; set; }
    }
}
